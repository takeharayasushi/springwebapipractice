package jp.takehara.SpringWebAPIPractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebApiPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebApiPracticeApplication.class, args);
	}
}
